Console.Write("Please enter a positive integer between 1 and 50: ");
int n = int.Parse(Console.ReadLine());

// Calculate the mystery number
int sum = 0;
for (int i = 1; i <= n; i++)
{
    sum = sum + DigitSum(i);
}
Console.WriteLine($"Mystery number to {n} is {sum}");

// Calculate the factorial of n
long factorial = Factorial(n);
Console.WriteLine($"The factorial of {n} is {factorial}");

// Write the Fibonacci sequence up to n
Console.Write("The Fibonacci sequence up to {0} is: 0, 1", n);
int a = 0, b = 1;
while (b <= n)
{
    Console.Write(", {0}", b);
    int temp = a + b;
    a = b;
    b = temp;
}
Console.WriteLine();

Console.Write("Is the number a prime number? {0}", IsPrime(n));


static int DigitSum(int n)
{
    int sum = 0;
    while (n > 0)
    {
        sum = sum + (n % 10);
        n = n / 10;
    }
    return sum;
}

static long Factorial(int n)
{
    long result = 1;
    for (int i = 2; i <= n; i++)
    {
        result = result * i;
    }
    return result;
}

static bool IsPrime(int n)
{
    if (n <= 1)
    {
        return false;
    }
    
    if (n <= 3)
    {
        return true;
    }

    if (n % 2 == 0 || n % 3 == 0)
    {
        return true;
    }

    int i = 5;
    while (i * i <= n)
    {
        if (n % i == 0 || n % (i + 2) == 0)
        {
            return false;
        }
        i += 6;
    }

    return true;
}
